# testcase

## Project setup
The app can be run in two way 

### using docker 
You can build a docker container from the DockerFile 
or pull the image `fred14/vue-fe-testcase` from ducker hub

### on local machine 

Clone the repository and run 
`npm install` then `npm run serve`

### Review of the Service
+ Contracts and Companies class should be in separate modules since the are two independent
service.

+ the `find` method must be implement with `Array.protype.find()` instead of 
`Array.prototype.filter()` since `id` are unique and also retrieving an array to return 
only the first element is not appropriate.

+ `Object.prototype.create()` and `Array.prototype.slice()` can be use
in place of `_clone` and `_cloneAll` methods the is no need to implement them 
