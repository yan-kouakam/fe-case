import users from '../data/users'

export class UserAuth {
  constructor () {
    this.users = users
  }

  async login (email, password) {
    return new Promise(async (resolve, reject) => {
      try {
        const user = await this._find(email)
        if (user.password !== password) {
          reject(new Error('Invalid password'))
        }
        return resolve(true)
      } catch (error) {
        reject(error)
      }
    })
  }

  registerUser (email, password, username) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const check = this.users.find((usr) => usr.email === email || usr.username === username)
        if (check) {
          reject(new Error('username or email already exists'))
        }
        this.users.push({ email, password, username })
        resolve(true)
      }, 500)
    })
  }

  _find (email) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const user = this.users.find((usr) => usr.email === email)
        if (!user) {
          reject(new Error(`User with email ${email} not found`))
        }

        resolve(user)
      }, 500)
    })
  }
}
