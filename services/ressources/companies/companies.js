import compData from '../../data/companies'
import Resource from '../resource'

export class Companies extends Resource {
  constructor () {
    super(compData)
  }
}
