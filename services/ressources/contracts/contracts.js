import contData from '../../data/contracts'
import Resource from '../resource'

export class Contracts extends Resource {
  constructor () {
    super(contData)
  }
}
