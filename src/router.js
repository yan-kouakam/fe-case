import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/companies',
      name: 'companies',
      component: () => import('./views/companies/Companies.vue')
    },
    { path: '/login',
      component: () => import('@/views/auth/Login.vue'),
      name: 'login'
    },
    { path: '/register',
      component: () => import('@/views/auth/Registration.vue'),
      name: 'register'
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '*', redirect: Home
    }
  ]
})
