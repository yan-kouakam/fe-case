import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import ContractsStore from './stores.modules/contracts/contracts.store'
import CompaniesStore from './stores.modules/companies/companies.store'
import UserStore from './stores.modules/users/users.store'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [
    createPersistedState({
      key: 'fe-testcase'
    })
  ],
  modules: {
    ContractsStore,
    CompaniesStore,
    UserStore
  }
})
