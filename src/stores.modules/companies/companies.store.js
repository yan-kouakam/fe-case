import { Companies } from '../../../services/ressources/companies/companies'

const companies = new Companies()

const state = {
  companies: []
}

const mutations = {
  'INIT-COMPANIES-LIST' (state, payload) {
    state.companies = payload
  }
}

const actions = {
  fetchCompaniesById: ({ state }, cid) => {
    const ctrs = state.companies.filter(ctr => ctr.cid === cid)
    return Promise.resolve(ctrs)
  },
  fetchAllCompanies: async ({ commit }) => {
    const companiesList = await companies.data.slice()
    commit('INIT-COMPANIES-LIST', companiesList)
    return Promise.resolve(companiesList)
  }
}

const getters = {
  getAllCompanies: state => state.companies
}

export default {
  state,
  mutations,
  actions,
  getters
}
