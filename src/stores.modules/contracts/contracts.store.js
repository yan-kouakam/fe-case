import { Contracts } from '../../../services/ressources/contracts/contracts'

const contracts = new Contracts()

const state = {
  contracts: []
}

const mutations = {
  'INIT-CONTRACTS' (state, payload) {
    state.contracts = payload
  },
  'UPDATE-CONTRACT' (state, contract) {
    const index = state.contracts.findIndex((ctr) => ctr.id === contract.id)
    if (index >= 0) {
      state.contracts[index] = contract
    }
  }
}

const actions = {
  fetchContractsByCompanyId: ({ state }, cid) => {
    const ctrs = state.contracts.filter(ctr => ctr.cid === cid)
    return Promise.resolve(ctrs)
  },
  fetchAllContracts: async ({ commit }) => {
    const contractList = await contracts.data.slice()
    commit('INIT-CONTRACTS', contractList)
  },
  updateContract: async ({ commit }, contract) => {
    try {
      const updatedContract = await contracts.update(contract)
      commit('UPDATE-CONTRACT', updatedContract)
      return updatedContract
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

const getters = {
  getAllContracts: state => state.contracts
}

export default {
  state,
  mutations,
  actions,
  getters
}
