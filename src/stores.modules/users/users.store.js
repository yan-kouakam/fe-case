import { UserAuth } from '../../../services/ressources/auth'

const Auth = new UserAuth()

const state = {
  isLogin: true,
  users: []
}

const mutations = {
  'LOGIN' (state) {
    state.isLogin = true
  },

  'LOGOUT' (state) {
    state.isLogin = false
  },

  'ADD-USER' (state, payload) {
    state.users.push(payload)
  },
  'LOAD-USER' (state, users) {
    state.users = users
  }
}

const actions = {
  login: async ({ commit }, { email, password }) => {
    try {
      const res = await Auth.login(email, password)
      if (res === true) {
        commit('LOGIN')
      }
      return res
    } catch (err) {
      return Promise.reject(err)
    }
  },
  logout: ({ commit }) => {
    commit('LOGOUT')
  },
  register: async ({ commit }, { email, password, username }) => {
    try {
      const res = await Auth.registerUser(email, password, username)
      if (res === true) {
        commit('ADD-USER', { email, password, username })
      }
      return res
    } catch (err) {
      return Promise.reject(err)
    }
  },
  loadUser: async ({ commit }) => {
    const users = await Auth.users
    commit('LOAD-USER', users)
  }
}

const getters = {
  getLoginStatus: state => state.isLogin
}

export default {
  state,
  mutations,
  actions,
  getters
}
